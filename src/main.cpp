// encoding: UTF-8
// main.cpp : A simple CLI interface for Google's CED (Compact Encoding Detection) library.
//
/**
 * MIT License
 *
 * Copyright (c) 2022 Nicola Tuveri <nic.tuv AT gmail.com>. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "config.h"
#include "buildinfo.h"

#include "compact_enc_det/compact_enc_det.h"

static void print_build_options(void);

bool verbose = false;

#ifndef PARTIAL_READ
# define PARTIAL_READ 0
#endif /* ifndef PARTIAL_READ */

#define MIN_INGEST_BYTES (2 << 23)
#define MAX_INGEST_BYTES 0x7ffff000
#define INITIAL_BUF_SIZE (MIN_INGEST_BYTES + 1)

// ============================================================================

static
Encoding detectEncoding(const char* text, size_t len, size_t *consumed_bytes, bool* pIsReliable)
{
  int bytes_consumed;
  int text_len = static_cast<int>(len);

  Encoding encoding = CompactEncDet::DetectEncoding(
      /* INPUTS */
          text, text_len,
          nullptr /* top-level-domain URL hint */,
          nullptr /* HTTP header charset hint */,
          nullptr /* <meta> charset hint */,
          UNKNOWN_ENCODING /* Encoding hint */,
          UNKNOWN_LANGUAGE /* Language hint */,
          CED_CORPUS /* corpus type */,
          false /* ignore_7bit */,

      /* OUTPUTS */
          // bytes_consumed says how much of text_len was actually examined
          &bytes_consumed,
          // is_reliable set true if the returned encoding is at least 2**10 time more
          // probable then the second-best encoding
          pIsReliable);

  if (verbose) {
      const char* rel = (*pIsReliable ? "reliable" : "NOT reliable");

      fprintf(stderr,
            "%s.\n\n",
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_INFO_STR)
            );

      print_build_options();

      fprintf(stderr,
              "Input size: %d bytes.\n"
              "Consumed %d bytes for Encoding detection.\n"
              "Encoding is: %s (%s) [%s].\n",
              text_len,
              bytes_consumed,
              EncodingName(encoding), MimeEncodingName(encoding), rel);
  }

  *consumed_bytes = static_cast<size_t>(bytes_consumed);
  return encoding;
}

static
bool read_all_stdin(const char **ppBuffer, size_t *pBufLen)
{
    char *buf = nullptr;

    buf = (char *) mmap(NULL, INITIAL_BUF_SIZE,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS,
            -1, 0);

    if (MAP_FAILED == buf) {
        int e = errno;
        const char *errmsg = strerror(e);
        fprintf(stderr, "\nCannot allocate ingest buffer: %s [%d]\n", errmsg, e);
        return false;
    }

    size_t read_bytes = 0;
    size_t allocated_bytes = INITIAL_BUF_SIZE;

    while (true) {
        size_t max;
        char * dst;

#if PARTIAL_READ == 0
        if (allocated_bytes - read_bytes - 1 < MIN_INGEST_BYTES) {
            /* remap into a bigger buffer */
            size_t newsize = allocated_bytes + (allocated_bytes >> 1);

            void *na = mremap(buf, allocated_bytes, newsize, MREMAP_MAYMOVE);

            if (MAP_FAILED == na) {
                int e = errno;
                const char *errmsg = strerror(e);
                fprintf(stderr, "\nCannot remap ingest buffer: %s [%d]\n", errmsg, e);
                return false;
            }

            buf = (char*)na;
            allocated_bytes = newsize;
        }
#endif
        max = allocated_bytes - read_bytes - 1;
        max = max > MAX_INGEST_BYTES ? MAX_INGEST_BYTES : max;
        dst = &buf[read_bytes];

        ssize_t ret = read(STDIN_FILENO, dst, max);

        if (-1 == ret) {
            int e = errno;
            const char *errmsg;

            switch (e) {
                case EAGAIN:
#if EAGAIN != EWOULDBLOCK
                case EWOULDBLOCK:
#endif
                case EINTR:
                    if (verbose) {
                        errmsg = strerror(e);
                        fprintf(stderr, "\nReading again on %s.\n", errmsg);
                    }
                    continue;
                    break;;
                default:
                    errmsg = strerror(e);
                    fprintf(stderr, "\nCannot allocate ingest buffer: %s [%d]\n", errmsg, e);
                    return false;
            }
        } else if (ret == 0) {
            /* EOF */
            buf[read_bytes] = '\0';
            break;
        } else {
            read_bytes += ret;
        }
    }

    if ( 0 != close(STDIN_FILENO) ) {
        int e = errno;
        const char *errmsg = strerror(e);
        fprintf(stderr, "\nFailure closing stdin: %s [%d]\n", errmsg, e);
        return false;
    }

    *ppBuffer = buf;
    *pBufLen = read_bytes;

    return true;
}

static
void release_buf(void *buffer, size_t buflen)
{
    buflen = buflen > 0 ? buflen : 1;
    int r = munmap(buffer, buflen);

    if (r != 0) {
        int e = errno;
        const char *errmsg = strerror(e);
        fprintf(stderr, "\nFailed to unmap the ingest buffer: %s [%d]\n", errmsg, e);
        exit(EXIT_FAILURE);
    }
}

// ============================================================================

static
void print_build_options(void)
{
    fprintf(stderr,
            "Build configuration:\n"
            "\n"
#if OUTPUT_COMPILER_INFO == 1
            "\tbuild type: %s\n"
            "\t%s\n"
            "\t%s\n"
            "\n"
#endif
            "\tOUTPUT_COMPILER_INFO: %34s\n"
            "\tPARTIAL_READ:         %34s\n"
            "\tCED_CORPUS:           %34s\n"
            "\n"
            "\tMIN_INGEST_BYTES:     %32zu B\n"
            "\tMAX_INGEST_BYTES:     %32zu B\n"
            "\tINITIAL_BUF_SIZE:     %32zu B\n"
            "\n",

#if OUTPUT_COMPILER_INFO == 1
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_BUILD_TYPE),
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_COMPILER),
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_COMPILER_FLAGS),
#endif
            OUTPUT_COMPILER_INFO == 1 ? "ON" : "OFF",
            PARTIAL_READ == 1 ? "ON" : "OFF",
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_CED_CORPUS),

            (size_t)MIN_INGEST_BYTES,
            (size_t)MAX_INGEST_BYTES,
            (size_t)INITIAL_BUF_SIZE
            );
}

static
void usage(const char *pname)
{
    fprintf(stderr,
            "%s.\n\n"
            "%s\n\n"
            "Usage: %s [-v | -h]\n\n",


            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_INFO_STR),
            CED_CLI_get_buildinfo(CED_CLI_BUILDINFO_DESCRIPTION),
            pname);

    print_build_options();
}


// ============================================================================
//
// Main
//
int main(int argc, char* argv[])
{
    int argcnt = 1;
    while (argcnt < argc) {
        if (strcmp(argv[argcnt], "-h") == 0) {
            argcnt++;
            usage(argv[0]);
            exit(EXIT_SUCCESS);
        } else if (strcmp(argv[argcnt], "-v") == 0) {
            argcnt++;
            verbose = true;
        } else {
            usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    const char *buffer;
    size_t buflen;

    if (!read_all_stdin(&buffer, &buflen)) {
        fprintf(stderr, "\nFailed to ingest standard input.\n");
        exit(EXIT_FAILURE);
    }

    bool is_reliable = false;
    size_t consumed_bytes;
    Encoding enc = detectEncoding(buffer, buflen, &consumed_bytes, &is_reliable);

#define SEP ", "
    printf("{"
            "\"EncodingName\": \"%s\"" SEP
            "\"MimeEncodingName\": \"%s\"" SEP
            "\"IsReliable\": %d" SEP
            "\"InputBytes\": %zu" SEP
            "\"ConsumedBytes\": %zu" // SEP
            "}\n",
            EncodingName(enc),
            MimeEncodingName(enc),
            is_reliable ? 1 : 0,
            buflen,
            consumed_bytes);

    release_buf((void*)buffer, buflen);

    return EXIT_SUCCESS;
}
// ============================================================================
