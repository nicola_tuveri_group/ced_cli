# CED_CLI

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

This is a simple CLI interface for the [Compact Encoding Detection (CED
)][CED@github] library by Google.

[CED@github]: https://github.com/google/compact_enc_det

> Compact Encoding Detection (CED for short) is a library written in C++
> that scans given raw bytes and detect the most likely text encoding.

This CLI interface simply ingests its _standard input_, and returns the
results offered by the library, in JSON format.

## How to build

~~~bash
cmake -DCMAKE_BUILD_TYPE=Release -DASAN=OFF -DPARTIAL_READ=OFF -DOUTPUT_COMPILER_INFO=ON -DCED_CORPUS=WEB_CORPUS -B ./build .
cmake --build ./build
~~~

## Example usage

~~~bash
# Note that the verbose output is on stderr, while stdout emits valid JSON
curl --silent 'https://www.gutenberg.org/files/28014/28014-0.txt' | iconv -c -f UTF-8 -t ISO-8859-2 | ./build/ced_cli -v | jq
ced_cli v0.1.3 (built on: Sun, 19 Jun 2022 22:49:45 +0000).
(libCED v2.2 [3ba472037e40a0acf6bb61b1727e99602cd71647]).

Build configuration:

        build type: Release
        compiler: "/usr/bin/c++" (id: "GNU", version: 9.4.0)
        compiler flags: " -Wno-narrowing -O3 -DNDEBUG   -Wall -Wextra -pedantic -Werror -std=c++11"

        OUTPUT_COMPILER_INFO:                                 ON
        PARTIAL_READ:                                        OFF
        CED_CORPUS:                    CompactEncDet::WEB_CORPUS

        MIN_INGEST_BYTES:                             16777216 B
        MAX_INGEST_BYTES:                           2147479552 B
        INITIAL_BUF_SIZE:                             16777217 B

Input size: 226409 bytes.
Consumed 1567 bytes for Encoding detection.
Encoding is: Latin2 (ISO-8859-2) [reliable].
{
  "EncodingName": "Latin2",
  "MimeEncodingName": "ISO-8859-2",
  "IsReliable": 1,
  "InputBytes": 226409,
  "ConsumedBytes": 1567
}
~~~

## License

This project is released under the MIT License.

Copyright (c) 2022 Nicola Tuveri <nic.tuv AT gmail.com>. All Rights Reserved.

See [LICENSE](LICENSE) for the full details.
