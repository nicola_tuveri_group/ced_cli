cmake_minimum_required(VERSION 3.13)

project(ced_cli
    VERSION 0.1.3
    DESCRIPTION
        "CLI interface for Google's Compact Encoding Detection library"
    HOMEPAGE_URL "https://gitlab.com/nicola_tuveri_group/ced_cli"
    LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# set build type to Release if not provided via command line
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type" FORCE)
endif()

add_compile_options(-Wall -Wextra -pedantic -Werror)

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-narrowing")
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-c++11-narrowing")
else()
    message(FATAL_ERROR "Unsupported compiler: ${CMAKE_CXX_COMPILER_ID}")
endif()

# output compiler information
if(CMAKE_CXX_COMPILER_LOADED)
  message(STATUS "Compiler path: ${CMAKE_CXX_COMPILER}")
  message(STATUS "Compiler ID: ${CMAKE_CXX_COMPILER_ID}")
  message(STATUS "Compiler version: ${CMAKE_CXX_COMPILER_VERSION}")
  message(STATUS "Compiler is part of GCC: ${CMAKE_COMPILER_IS_GNUCXX}")
endif()

# option for outputting compiler information at runtime
option(OUTPUT_COMPILER_INFO
      "Output compiler information when launching the main executable." ON)

option(PARTIAL_READ "If enabled, only part of the input is read." OFF)

option(ASAN "Enable to build with AddressSanitizer." OFF)

if(${ASAN})
    message(STATUS "Enabling AddressSanitizer")
    add_compile_options(-fsanitize=address -fno-omit-frame-pointer)
    add_link_options(-fsanitize=address -static-libasan)
endif()


set(CED_CORPUS "WEB_CORPUS" CACHE STRING
    [=[Corpus type for DetectEncoding. Can be one of `{WEB,XML,QUERY,EMAIL}_CORPUS` or `NUM_CORPA`.]=]
)
message(STATUS "DetectEncoding corpus: CompactEncDet::${CED_CORPUS}")

# generate configuration file
configure_file(src/config.h.in "${PROJECT_BINARY_DIR}/config.h" @ONLY)

set(CED_CLI_SRC
    src/main.cpp
    ${PROJECT_BINARY_DIR}/config.h
    ${PROJECT_BINARY_DIR}/buildinfo.cpp
    ${PROJECT_BINARY_DIR}/buildinfo.h
)
add_executable(ced_cli ${CED_CLI_SRC})
target_include_directories(ced_cli
    PRIVATE "${PROJECT_BINARY_DIR}"
)

add_custom_command(
    OUTPUT ${PROJECT_BINARY_DIR}/buildinfo.cpp
    COMMAND
        ruby "${PROJECT_SOURCE_DIR}/src/buildinfo.cpp.rb"
        "$(CXX_DEFINES) $(CXX_FLAGS)"
        "${CMAKE_CXX_COMPILER}"
        "${CMAKE_CXX_COMPILER_ID}"
        "${CMAKE_CXX_COMPILER_VERSION}"
        > ${PROJECT_BINARY_DIR}/buildinfo.cpp
    MAIN_DEPENDENCY "${PROJECT_SOURCE_DIR}/src/buildinfo.cpp.rb"
    DEPENDS "${PROJECT_BINARY_DIR}/buildinfo.out"
    VERBATIM
)

add_custom_command(
    OUTPUT ${PROJECT_BINARY_DIR}/buildinfo.h
    COMMAND
        ruby ${PROJECT_SOURCE_DIR}/src/buildinfo.h.rb
        > ${PROJECT_BINARY_DIR}/buildinfo.h
    MAIN_DEPENDENCY "${PROJECT_SOURCE_DIR}/src/buildinfo.h.rb"
    VERBATIM
)


add_subdirectory(compact_enc_det) # <--- libCED

target_link_libraries(ced_cli libCED)
get_target_property(LIBCED_COMMITTISH libCED COMMITTISH)
message(STATUS "libCED COMMITTISH: ${LIBCED_COMMITTISH}")

configure_file("src/buildinfo.out.in" "${PROJECT_BINARY_DIR}/buildinfo.out" @ONLY)
